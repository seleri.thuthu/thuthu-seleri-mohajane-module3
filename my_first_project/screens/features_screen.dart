import 'package:flutter/material.dart';
import 'package:my_first_project/screens/interior_features_screen.dart';
import 'package:my_first_project/screens/exterior_features.dart';


void main() => runApp(const FeaturesScreen());

class FeaturesScreen extends StatelessWidget {
  const FeaturesScreen({Key? key}) : super(key: key);

  static const String _title = 'Features Page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: Scaffold(
        appBar: AppBar(title: const Text(_title),
        ),  
        body: const MyStatefulWidget(),
      ),
    );
  }
}

class MyStatefulWidget extends StatefulWidget {
  const MyStatefulWidget({Key? key}) : super(key: key);

  @override
  State<MyStatefulWidget> createState() => _MyStatefulWidgetState();
}

class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  @override
  Widget build(BuildContext context) {
    final ButtonStyle style =
        ElevatedButton.styleFrom(textStyle: const TextStyle(fontSize: 20));

    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ElevatedButton(
            style: style,
            onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const InteriorFeatures()),
            );
          },
            child: const Text('Interior'),
          ),
          const SizedBox(height: 30),
          ElevatedButton(
            style: style,
            onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const ExteriorFeatures()),
            );
          },
            child: const Text('Exterior'),
          ),
        ],
      ),
    );
  }
}
