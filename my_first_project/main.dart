import 'package:flutter/material.dart';
import 'package:my_first_project/screens/log_in_screen.dart';

void main() => runApp(const MyApp());
 
class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
   static const String _title = 'Login Page';
 
  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: _title,
      home: LogInScreen(),
    );
  }
}
 
