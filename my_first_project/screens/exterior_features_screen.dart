import 'package:flutter/material.dart';
import 'package:my_first_project/screens/profile_edit_screen.dart';

class ExteriorFeatures extends StatelessWidget {
  const ExteriorFeatures({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My First Project',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Exterior Features Page'),
          leading: GestureDetector(
          child: const Icon( Icons.arrow_back_ios, color: Colors.black,  ),
          onTap: () {
            Navigator.pop(context);
          } ,
        ),
        ),
        body: const Center(
          child: Text('Exterior features will appear on this page'),
        ),
        floatingActionButton: FloatingActionButton(
          tooltip: 'Do to Next Page',
          child: const Icon(Icons.navigate_next),
        onPressed: (){
          Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const ProfileEditScreen()));
        }
      ),
    ),
  );
 }
}    